﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ModuleLibrary.Versions.v_1_0_0.ViewModels;

namespace ModuleLibrary.Versions.v_1_0_0.Views
{
  /// <summary>
  /// Interaction logic for the ModuleCfgView.xaml
  /// </summary>
  public partial class ModuleCfgView : UserControl
  {
    ViewModels.Module_ViewModel newCfg;
    string pathToConfigFile;
    string absPathModuleBaseFolder;

  /// <summary>
  /// Constructor
  /// </summary>
  /// <param name="pathToCfgFile">Absolute Path to configuration file</param>
  /// <param name="absPathToModuleBaseFolder">Absolute Path to module base folder</param>
  public ModuleCfgView(string pathToCfgFile, string absPathToModuleBaseFolder)
    {
      /* Check if config file path is an rooted one */
      if (true == System.IO.Path.IsPathRooted(pathToCfgFile))
      {
        pathToConfigFile = pathToCfgFile;
      }
      else
      {
        pathToConfigFile = absPathToModuleBaseFolder + pathToCfgFile;
      }

      newCfg = new ViewModels.Module_ViewModel(pathToCfgFile, absPathToModuleBaseFolder);

      InitializeComponent();

      /* Set Data Context where data is binded to*/
      this.DataContext = newCfg;
    }

    private void B_AddNewUartConfig_Click(object sender, RoutedEventArgs e)
    {
      newCfg.AddNewUartCfg();
    }

    private void B_RemoveSelectedUartConfig_Click(object sender, RoutedEventArgs e)
    {
      UartCfgVM removedBlock = (UartCfgVM)DG_UartCfgList.SelectedItem;
      newCfg.RemoveSpecificUartCfg(removedBlock);
    }
  }
}
