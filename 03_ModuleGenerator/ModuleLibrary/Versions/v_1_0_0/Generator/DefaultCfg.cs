﻿/*****************************************************************************************************************************
 * @file        DefaultCfg.cs                                                                                                *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Default Module Class                                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generic;
using ModuleLibrary.Versions.v_1_0_0.Models;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Generator
{
  internal class DefaultCfgGenerator
  {
    private Models.UartXml xmlCfg;
    private string pathToConfiguratioFile;
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be set to default </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    public DefaultCfgGenerator(Models.UartXml cfgFile, string pathToCfgFile)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to set default configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType CreateDefaultConfiguration()
    {
      info.AddInfoMsg(DefResources.InfoMsg_StartDefaultCfgCreation + DefResources.CfgFileMajorVersion +
        "." + DefResources.CfgFileMinorVersion + "." + DefResources.CfgFilePatchVersion);
      info.AddLogMsg(DefResources.LogMsg_StartDefaultCfgCreation);

      /* Create default configuration*/
      if (null == xmlCfg)
      {
        xmlCfg = new Models.UartXml();
      }

      // Create Configuration File Version
      xmlCfg.xmlFileVersion.MajorVersion = Convert.ToUInt16(DefResources.CfgFileMajorVersion);
      xmlCfg.xmlFileVersion.MinorVersion = Convert.ToUInt16(DefResources.CfgFileMinorVersion);
      xmlCfg.xmlFileVersion.PatchVersion = Convert.ToUInt16(DefResources.CfgFilePatchVersion);

      // Create Det Module Id
      xmlCfg.detModuleID = Convert.ToUInt16(DefResources.DefaultModuleId);

      // Create module Det usage state
      if (DefResources.DefaultModuleUsage == SystemState.STD_ON.ToString())
        xmlCfg.detModuleUsage = SystemState.STD_ON;
      else
        xmlCfg.detModuleUsage = SystemState.STD_OFF;

      xmlCfg.uartModuleCfgList = new List<uartModuleCfg>();
      uartModuleCfg uartCfg = new uartModuleCfg();
      uartCfg.uartBaudRate = Convert.ToUInt16(DefResources.DefUartDefaultBaudRate);
      uartCfg.uartDataWidth = Convert.ToByte(DefResources.DefUartDefaultDataWith);
      uartCfg.uartMaxRxBytes = Convert.ToUInt16(DefResources.DefUartDefaultMaxRxBytes);
      uartCfg.uartModuleId = Convert.ToByte(DefResources.DefUartDefaultUartModuleId);
      uartCfg.uartRxTimeout = Convert.ToUInt32(DefResources.DefUartDefaultRxTimeout);
      uartCfg.uartTxTimeout = Convert.ToUInt32(DefResources.DefUartDefaultTxTimeout);

      /* Config Hardware flow control type */
      uartCfg.uartHwFlowControl = Uart_HwFlowControlType.MCAL_UART_HWCONTROL_NONE;

      /* Config Hardware stop bits  */
      uartCfg.uartStopBits = Uart_StopBitsType.MCAL_UART_STOPBIT_1;

      /* Config Hardware parity */
      uartCfg.uartParity = Uart_ParityType.MCAL_UART_PARITY_NONE;

      /* Config Hardware mode */
      uartCfg.uartMode = Uart_ModeType.MCAL_UART_MODE_TXRX;

      /* Config Transmission type */
      uartCfg.uartTransmissionType = Uart_TransmissionType.MCAL_UART_TRANSMISSION_POLLING;
      xmlCfg.uartModuleCfgList.Add(uartCfg);

      /*-----------------------------------------------------------------------------------------------------------*/
      /* Set default Frame End Pattern */
      xmlCfg.uartFrameEndPatternElements = new Byte[2];
      xmlCfg.uartFrameEndPatternElements[0] = 0x0D;
      xmlCfg.uartFrameEndPatternElements[1] = 0x0A;

      /* Set default Mainfunction cycle time */
      xmlCfg.miBMainfunctionCycleTimeMs = Convert.ToUInt16(DefResources.DefUartMainfunctionCycleTime);

      info.AddLogMsg(DefResources.LogMsg_CreationDone);

      return info;
    }
  }
}
/**
 * @}
 */
