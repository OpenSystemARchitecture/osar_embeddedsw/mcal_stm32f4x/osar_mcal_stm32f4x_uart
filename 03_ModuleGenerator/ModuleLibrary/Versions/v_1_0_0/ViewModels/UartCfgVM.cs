﻿/*****************************************************************************************************************************
 * @file        UartCfgVM.cs                                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        03.01.2020                                                                                                   *
 * @brief       Implementation of the View Model from the Module Xml Cfg Class                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using ModuleLibrary.Versions.v_1_0_0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  public class UartCfgVM : BaseViewModel
  {
    uartModuleCfg usedUartConfig;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="uartCfg"></param>
    /// <param name="storeCfg"></param>
    public UartCfgVM(uartModuleCfg uartCfg, StoreConfiguration storeCfg) :base(storeCfg)
    {
      usedUartConfig = uartCfg;
    }

    /// <summary>
    /// Constructor >> Create a defaulf uart config
    /// </summary>
    /// <param name="storeCfg"></param>
    public UartCfgVM(StoreConfiguration storeCfg) : base(storeCfg)
    {
      usedUartConfig = new uartModuleCfg();
      usedUartConfig.uartModuleId = 255;
      usedUartConfig.uartBaudRate = 9600;
      usedUartConfig.uartDataWidth = 8;
      usedUartConfig.uartTxTimeout = 5;
      usedUartConfig.uartRxTimeout = 5;
      usedUartConfig.uartMaxRxBytes = 100;

    }

    /// <summary>
    /// Interface for the uart configuration
    /// </summary>
    public uartModuleCfg UartConfig
    { 
      get => usedUartConfig; 
      set => usedUartConfig = value; 
    }

    /// <summary>
    /// Interface for the attribute usedUartConfig.uartModuleId
    /// </summary>
    public Byte UartModuleId
    {
      get => usedUartConfig.uartModuleId;
      set => usedUartConfig.uartModuleId = value;
    }

    /// <summary>
    /// Interface for the attribute usedUartConfig.uartDataWidth
    /// </summary>
    public Byte UartDataWidth
    {
      get => usedUartConfig.uartDataWidth;
      set => usedUartConfig.uartDataWidth = value;
    }

    /// <summary>
    /// Interface for the attribute usedUartConfig.uartBaudRate
    /// </summary>
    public UInt32 UartBaudRate
    {
      get => usedUartConfig.uartBaudRate;
      set => usedUartConfig.uartBaudRate = value;
    }

    /// <summary>
    /// Interface for the attribute usedUartConfig.uartTxTimeout
    /// </summary>
    public UInt32 UartTxTimeout
    {
      get => usedUartConfig.uartTxTimeout;
      set => usedUartConfig.uartTxTimeout = value;
    }

    /// <summary>
    /// Interface for the attribute usedUartConfig.uartRxTimeout
    /// </summary>
    public UInt32 UartRxTimeout
    {
      get => usedUartConfig.uartRxTimeout;
      set => usedUartConfig.uartRxTimeout = value;
    }

    /// <summary>
    /// Interface for the attribute usedUartConfig.uartMaxRxBytes
    /// </summary>
    public UInt16 UartMaxRxBytes
    {
      get => usedUartConfig.uartMaxRxBytes;
      set => usedUartConfig.uartMaxRxBytes = value;
    }

    /// <summary>
    /// Interface for the attribute usedUartConfig.uartStopBits
    /// </summary>
    public Uart_StopBitsType UartStopBits
    {
      get => usedUartConfig.uartStopBits;
      set => usedUartConfig.uartStopBits = value;
    }

    /// <summary>
    /// Interface for the attribute usedUartConfig.uartHwFlowControl
    /// </summary>
    public Uart_HwFlowControlType UartHwFlowControl
    {
      get => usedUartConfig.uartHwFlowControl;
      set => usedUartConfig.uartHwFlowControl = value;
    }

    /// <summary>
    /// Interface for the attribute usedUartConfig.uartHwFlowControl
    /// </summary>
    public Uart_ParityType UartParity
    {
      get => usedUartConfig.uartParity;
      set => usedUartConfig.uartParity = value;
    }

    /// <summary>
    /// Interface for the attribute usedUartConfig.uartMode
    /// </summary>
    public Uart_ModeType UartMode
    {
      get => usedUartConfig.uartMode;
      set => usedUartConfig.uartMode = value;
    }

    /// <summary>
    /// Interface for the attribute usedUartConfig.uartTransmissionType
    /// </summary>
    public Uart_TransmissionType UartTransmissionType
    {
      get => usedUartConfig.uartTransmissionType;
      set => usedUartConfig.uartTransmissionType = value;
    }
  }
}
