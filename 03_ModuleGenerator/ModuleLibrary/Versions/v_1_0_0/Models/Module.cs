﻿/*****************************************************************************************************************************
 * @file        Module.cs                                                                                                    *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the Module Configuration Data Model                                                        *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.Models
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;
using System.Xml.Serialization;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Models
{
  /**
   * @brief           Possible Uart Parity types
   */
  public enum Uart_ParityType
  {
    MCAL_UART_PARITY_NONE,
    MCAL_UART_PARITY_EVEN,
    MCAL_UART_PARITY_ODD
  }

  /**
    * @brief           Possible Uart modes
    */
  public enum Uart_ModeType
  {
    MCAL_UART_MODE_TX,
    MCAL_UART_MODE_RX,
    MCAL_UART_MODE_TXRX
  }

  /**
    * @brief           Uart transmission type
    */
  public enum Uart_TransmissionType
  {
    MCAL_UART_TRANSMISSION_POLLING,                 /*!< Used transmission type == polling */
    MCAL_UART_TRANSMISSION_INTERRUPT                /*!< Used transmission type == interrupt */
  }

  /**
    * @brief           Uart Hardware flow control type
    */
  public enum Uart_HwFlowControlType
  {
    MCAL_UART_HWCONTROL_NONE,                       /*!< No hardware flow control*/
    MCAL_UART_HWCONTROL_RTS,                        /*!< Hardware flow control using RTS */
    MCAL_UART_HWCONTROL_CTS,                        /*!< Hardware flow control using CTS */
    MCAL_UART_HWCONTROL_RTS_CTS                     /*!< Hardware flow control using RTS and CTS */
  }

  /**
    * @brief           Uart Stop Bits
    */
  public enum Uart_StopBitsType
  {
    MCAL_UART_STOPBIT_1,                            /*!< Use 1 stop bit  */
    MCAL_UART_STOPBIT_1_5,                          /*!< Use 1.5 stop bits  */
    MCAL_UART_STOPBIT_2                             /*!< Use 2 stop bits  */
  }

  public struct uartModuleCfg
  {
    public Byte uartModuleId;                              /*!< Id of the UART HW Module which shall be used */
    public Byte uartDataWidth;                             /*!< Width an Uart Word >> Could be 8 or 9 */
    public UInt32 uartBaudRate;                            /*!< Baudrate which shall be used during the uart transmission */
    public UInt32 uartTxTimeout;                           /*!< Used timeout of a polling tx transmission in ms */
    public UInt32 uartRxTimeout;                           /*!< Used timeout of a polling rx transmission in ms */
    public UInt16 uartMaxRxBytes;                          /*!< Maximum rx bytes which could be received in one rx transmission */
    public Uart_StopBitsType uartStopBits;                 /*!< Cnt of start bits for the uart transmission >> Could be 1 or 2 */
    public Uart_HwFlowControlType uartHwFlowControl;       /*!< Uart hardware flow control */
    public Uart_ParityType uartParity;                     /*!< Parity which shall be used during the uart transmission */
    public Uart_ModeType uartMode;                         /*!< Transmission mode which shall be used during the uart transmission */
    public Uart_TransmissionType uartTransmissionType;     /*!< Which transmission type shall be used. */
  }

  /**
    * @brief    Uart module xsd class to generate an xml style sheet
    */
  public class UartXml
  {
    public XmlFileVersion xmlFileVersion;
    public UInt16 detModuleID;
    public SystemState detModuleUsage;
    public UInt16 miBMainfunctionCycleTimeMs;
    public List<uartModuleCfg> uartModuleCfgList;

    [XmlElement(DataType = "hexBinary")]
    public Byte[] uartFrameEndPatternElements;
  }
}
/**
 * @}
 */