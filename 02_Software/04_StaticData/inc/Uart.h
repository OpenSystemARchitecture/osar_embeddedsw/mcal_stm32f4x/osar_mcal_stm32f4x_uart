/*****************************************************************************************************************************
 * @file        Uart.h                                                                                                       *
 * @author      OSAR Team Sebastian Reinemuth                                                                                *
 * @date        11.03.2018 10:25:43                                                                                          *
 * @brief       Implementation of definitions / interface function prototypes / datatypes and generic module interface       *
 *              informations of the "Uart" module.                                                                           *
 *                                                                                                                           *
 * @details     The Uart module is the OSAR abstraction module for the external provided Hardware Abstraction files. It      *
 *              interacts with the OSAR Basis Software Modules (BSW) and with the HAL Software (Hardware Abstraction Layer). *
 *              The Uart Module implement two principal processing modes. The polling mode and the interrupt mode.           *
 *                                                                                                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
#ifndef __UART_H
#define __UART_H

/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Uart
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Uart_Types.h"
#include "Uart_PBCfg.h"
#include "UartIf.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfunction.
 */
void Uart_InitMemory( void );

/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void Uart_Init( void );

/**
 * @brief           Module global mainfunction.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called form the actual runtime environment within an fixed cycle.
 */
void Uart_Mainfunction( void );

/* ######################################################################################################################## */
/* ######################################## UART PROVIDED HIGH LEVEL INTERFACES ########################################### */
/* ######################################################################################################################## */
/**
 * @brief           API function to set a transmit data frame
 * @param[in]       UartIf_TxFrameType   Uart layer frame data
 * @retval          Std_ReturnType
 *                  >> E_OK
 *                  >> E_NOT_OK
 *                  >> E_PENDING
 *                  >> E_SKIPPED
 * @details         This function could be used to set a new transmit data frame from the uart module
 */
Std_ReturnType Uart_Transmit(UartIf_TxFrameType frameData);

/* ######################################################################################################################## */
/* ###################################### UART PROVIDED SYSTEM LEVEL INTERFACES ########################################### */
/* ######################################################################################################################## */
/**
 * @brief           Interrupt Service Routine for the UART 1
 * @param[in]       None
 * @retval          None
 * @note            Call the HAL interrupt handler
 */
void USART1_IRQHandler(void);

/**
 * @brief           Interrupt Service Routine for the UART 2
 * @param[in]       None
 * @retval          None
 * @note            Call the HAL interrupt handler
 */
void USART2_IRQHandler(void);
/**
 * @brief           Interrupt Service Routine for the UART 3
 * @param[in]       None
 * @retval          None
 * @note            Call the HAL interrupt handler
 */
void USART3_IRQHandler(void);

/**
 * @brief           Interrupt Service Routine for the UART 4
 * @param[in]       None
 * @retval          None
 * @note            Call the HAL interrupt handler
 */
void UART4_IRQHandler(void);

/**
 * @brief           Interrupt Service Routine for the UART 5
 * @param[in]       None
 * @retval          None
 * @note            Call the HAL interrupt handler
 */
void UART5_IRQHandler(void);

/**
 * @brief           Interrupt Service Routine for the UART 6
 * @param[in]       None
 * @retval          None
 * @note            Call the HAL interrupt handler
 */
void USART6_IRQHandler(void);

/* ######################################################################################################################## */
/* ####################################### PRIVATE UART SYSTEM LEVEL INTERFACES ########################################### */
/* ######################################################################################################################## */
/**
 * @brief           Callback function from the HAL Uart Module RX Interrupts
 * @param[in]       Uart Handle Type Def
 * @retval          None
 */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart);

/**
 * @brief           Callback function from the HAL Uart Module for RX Interrupts
 * @param[in]       Uart Handle Type Def
 * @retval          None
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __UART_H*/
