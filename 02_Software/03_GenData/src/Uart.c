/*****************************************************************************************************************************
 * @file        Uart.c                                                                                                       *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        20.03.2019 11:44:03                                                                                          *
 * @brief       Generated Rte Module Interface file: Uart.c                                                                  *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.10                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Uart 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_Uart.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of USER include and definition area           << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "Uart.h"
#include "stm32f4xx_hal.h"
#include "string.h"

#define MAX_AVAILABLE_UART_MODULES    6

#define Uart_START_SEC_NOINIT_VAR
#include "Uart_MemMap.h"
UartIf_RxFrameType rxFrameData[MAX_AVAILABLE_UART_MODULES];
uint16 rxFrameDataLengthCounter[MAX_AVAILABLE_UART_MODULES];
uint8 frameByteData[MAX_AVAILABLE_UART_MODULES][UART_GLOBAL_MAX_RX_ELEMENTS];
#define Uart_STOP_SEC_NOINIT_VAR
#include "Uart_MemMap.h"

/**
 * @brief           Helper function to fill a buffer with default values
 * @param[in]       uint8* Pointer to buffer
 * @param[in]       uint16 Buffer Length
 * @retval          none
 */
void hlpUartFillBufferDefaultValues(uint8* pBuffer, uint16 length);

/**
 * @brief           Helper function to search for an end pattern
 * @param[in]       uint8* Pointer to buffer
 * @param[in]       uint16 Buffer Length
 * @retval          uint16 Length of buffer data with end pattern
 */
uint16 hlpUartFindBufferEndPattern(uint8* pBuffer, uint16 length);

/**
 * @brief           Helper function check for received data within interrupt processing
 * @param[in]       uint8 module id
 * @retval          None
 */
void hlpUartCheckForRxFrameEndPatternInIsr(uint8 moduleId);
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           End of USER include and definition area            << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map functions into code memory ---------------------------------------------*/
#define Uart_START_SEC_CODE
#include "Uart_MemMap.h"
/**
 * @brief           Rte generated module initialization function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: System Init
 * @uuid            d7c5716a472711e9b210d663bd873d93
 */
VAR(void) Uart_Init( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  uint8 idx;

  /* Initialize the different Uart modules */
  for(idx = 0; idx < UART_CNT_OF_USED_MODULES; idx++)
  {

    /* >>>>> Select correct Module <<<<< */
    switch (uartHwModuleCfg[idx].uartModuleId)
    {
    case 0:
    halUartHdlTypes[idx].Instance = USART1;
    break;

    case 1:
    halUartHdlTypes[idx].Instance = USART2;
    break;

    case 2:
    halUartHdlTypes[idx].Instance = USART3;
    break;

    case 3:
    halUartHdlTypes[idx].Instance = UART4;
    break;

    case 4:
    halUartHdlTypes[idx].Instance = UART5;
    break;

    case 5:
    halUartHdlTypes[idx].Instance = USART6;
    break;

    default:
  #if (STD_ON == UART_MODULE_USE_DET)
      Det_ReportError(UART_DET_MODULE_ID, UART_E_GENERIC_PROGRAMMING_FAILURE);
  #endif
      break;
    }

    /* >>>>> Set used Baud rate <<<<< */
    halUartHdlTypes[idx].Init.BaudRate = uartHwModuleCfg[idx].uartBaudRate;

    /* >>>>> Set used data bits <<<<< */
    switch (uartHwModuleCfg[idx].uartDataWidth)
    {
    case 8:
      halUartHdlTypes[idx].Init.WordLength = USART_WORDLENGTH_8B;
      break;

    case 9:
      halUartHdlTypes[idx].Init.WordLength = USART_WORDLENGTH_9B;
      break;

    default:
  #if (STD_ON == UART_MODULE_USE_DET)
      Det_ReportError(UART_DET_MODULE_ID, UART_E_GENERIC_PROGRAMMING_FAILURE);
  #endif
      break;
    }

    /* >>>>> Set Stop bits <<<<< */
    switch (uartHwModuleCfg[idx].uartStopBits)
    {
    case MCAL_UART_STOPBIT_1:
      halUartHdlTypes[idx].Init.StopBits = UART_STOPBITS_1;
      break;

    case MCAL_UART_STOPBIT_1_5:
      halUartHdlTypes[idx].Init.StopBits = USART_STOPBITS_1_5;
      break;

    case MCAL_UART_STOPBIT_2:
      halUartHdlTypes[idx].Init.StopBits = USART_STOPBITS_2;
      break;

    default:
  #if (STD_ON == UART_MODULE_USE_DET)
      Det_ReportError(UART_DET_MODULE_ID, UART_E_GENERIC_PROGRAMMING_FAILURE);
  #endif
      break;
    }

    /* >>>>> Set Parity <<<<< */
    switch (uartHwModuleCfg[idx].uartParity)
    {
    case MCAL_UART_PARITY_NONE:
      halUartHdlTypes[idx].Init.Parity = UART_PARITY_NONE;
      break;

    case MCAL_UART_PARITY_EVEN:
      halUartHdlTypes[idx].Init.Parity = USART_PARITY_EVEN;
      break;

    case MCAL_UART_PARITY_ODD:
      halUartHdlTypes[idx].Init.Parity = USART_PARITY_ODD;
      break;

    default:
  #if (STD_ON == UART_MODULE_USE_DET)
      Det_ReportError(UART_DET_MODULE_ID, UART_E_GENERIC_PROGRAMMING_FAILURE);
  #endif
      break;
    }

    /* >>>>> Set Mode <<<<< */
    switch (uartHwModuleCfg[idx].uartMode)
    {
    case MCAL_UART_MODE_RX:
      halUartHdlTypes[idx].Init.Mode = USART_MODE_RX;
      break;

    case MCAL_UART_MODE_TX:
      halUartHdlTypes[idx].Init.Mode = USART_MODE_TX;
      break;

    case MCAL_UART_MODE_TXRX:
      halUartHdlTypes[idx].Init.Mode = USART_MODE_TX_RX;
      break;

    default:
  #if (STD_ON == UART_MODULE_USE_DET)
      Det_ReportError(UART_DET_MODULE_ID, UART_E_GENERIC_PROGRAMMING_FAILURE);
  #endif
    break;
    }

    /* >>>>> Set Hardware flow control <<<<< */
    switch (uartHwModuleCfg[idx].uartHwFlowControl)
    {
    case MCAL_UART_HWCONTROL_NONE:
      halUartHdlTypes[idx].Init.HwFlowCtl = UART_HWCONTROL_NONE;
      break;

    case MCAL_UART_HWCONTROL_CTS:
      halUartHdlTypes[idx].Init.HwFlowCtl = UART_HWCONTROL_CTS;
      break;

    case MCAL_UART_HWCONTROL_RTS:
      halUartHdlTypes[idx].Init.HwFlowCtl = UART_HWCONTROL_RTS;
      break;

    case MCAL_UART_HWCONTROL_RTS_CTS:
      halUartHdlTypes[idx].Init.HwFlowCtl = UART_HWCONTROL_RTS_CTS;
      break;

    default:
  #if (STD_ON == UART_MODULE_USE_DET)
      Det_ReportError(UART_DET_MODULE_ID, UART_E_GENERIC_PROGRAMMING_FAILURE);
  #endif
    break;
    }

    /* >>>>> Set Oversampling <<<<< */
    halUartHdlTypes[idx].Init.OverSampling = UART_OVERSAMPLING_16;

    /* >>>>> Initialize UART Mode <<<<< */
    HAL_UART_Init(&halUartHdlTypes[idx]);
    __HAL_UART_ENABLE(&halUartHdlTypes[idx]);

    /* >>>>> Set Transmission mode <<<<< */
    switch(uartHwModuleCfg[idx].uartTransmissionType)
    {
    case MCAL_UART_TRANSMISSION_INTERRUPT:
      HAL_UART_Receive_IT(&halUartHdlTypes[idx], &rxFrameData[uartHwModuleCfg[idx].uartModuleId].pData[rxFrameDataLengthCounter[uartHwModuleCfg[idx].uartModuleId]], 1);   //Activate UART receive interrupt every time
      break;

    case MCAL_UART_TRANSMISSION_POLLING:
      //NOTE >> Nothing to do in polling mode
      break;

    default:
  #if (STD_ON == UART_MODULE_USE_DET)
        Det_ReportError(UART_DET_MODULE_ID, UART_E_GENERIC_PROGRAMMING_FAILURE);
  #endif
      break;
    }
  }
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module cyclic function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: Cyclic 100ms
 * @uuid            98365b4c473911e9b210d663bd873d93
 */
VAR(void) Uart_Mainfunction( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  uint8 idx = 0;
  uint16 tempBufferDataLength;
  HAL_StatusTypeDef statusType;
  uint8 dummyData[UART_GLOBAL_MAX_RX_ELEMENTS];

  /* Perform receive actions as polling and interrupt for all modules */
  for(idx = 0; idx < UART_CNT_OF_USED_MODULES; idx++)
  {
    switch(uartHwModuleCfg[idx].uartTransmissionType)
    {
      /* ---------------------------------------------------------------------------------------------- */
      /* -------------------------- Processing interrupt transmission --------------------------------- */
      /* ---------------------------------------------------------------------------------------------- */
      case MCAL_UART_TRANSMISSION_INTERRUPT:
        //NOTE >> Nothing to do in polling mode >> Receive of data would be handled in interrupt mode
        break;

      /* ---------------------------------------------------------------------------------------------- */
      /* -------------------------- Processing polling transmission ----------------------------------- */
      /* ---------------------------------------------------------------------------------------------- */
      case MCAL_UART_TRANSMISSION_POLLING:
        /* Set dummy data */
        rxFrameData[uartHwModuleCfg[idx].uartModuleId].frameState = UART_FRAME_OK;
        memcpy(&rxFrameData[uartHwModuleCfg[idx].uartModuleId].pData[0], &mcalUartPduEndPattern[0], UART_FRAME_END_PATTERN_LENGTH);
        rxFrameData[uartHwModuleCfg[idx].uartModuleId].cntOfData = uartHwModuleCfg[idx].uartMaxRxBytes;
        rxFrameData[uartHwModuleCfg[idx].uartModuleId].moduleID = idx;

        /* Start reception of data */
        statusType = HAL_UART_Receive(&halUartHdlTypes[idx], rxFrameData[uartHwModuleCfg[idx].uartModuleId].pData, rxFrameData[uartHwModuleCfg[idx].uartModuleId].cntOfData, uartHwModuleCfg[idx].uartRxTimeout);
        if( (HAL_OK == statusType) || (HAL_TIMEOUT == statusType) )
        {
          /* Search for end pattern */
          tempBufferDataLength = hlpUartFindBufferEndPattern(rxFrameData[uartHwModuleCfg[idx].uartModuleId].pData, rxFrameData[uartHwModuleCfg[idx].uartModuleId].cntOfData);

          /* Check if end pattern has been found */
          if(0xFFFF != tempBufferDataLength)
          {rxFrameData[uartHwModuleCfg[idx].uartModuleId].cntOfData = tempBufferDataLength;} /* Buffer end pattern has been found >> Set new length */

          /* Check if Data has been received */
          if(UART_FRAME_END_PATTERN_LENGTH == rxFrameData[uartHwModuleCfg[idx].uartModuleId].cntOfData)
          {
            break;  /* No data has been received */
          }
          else
          {
            /* Data has been received so report the to interface module */
            UartIf_RxNotification(rxFrameData[uartHwModuleCfg[idx].uartModuleId]);
          }
        }
  #if (STD_ON == UART_MODULE_USE_DET)
        if(HAL_BUSY == statusType)
        { Det_ReportError(UART_DET_MODULE_ID, UART_E_UNEXPECTED_HAL_MODULE_STATE); }
  #endif
        break;

      /* ---------------------------------------------------------------------------------------------- */
      /* ----------------------------- ERROR >> Invalid configuration --------------------------------- */
      /* ---------------------------------------------------------------------------------------------- */
      default:
  #if (STD_ON == UART_MODULE_USE_DET)
         Det_ReportError(UART_DET_MODULE_ID, UART_E_GENERIC_PROGRAMMING_FAILURE);
  #endif
        break;
    }
  }
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

#define Uart_STOP_SEC_CODE
#include "Uart_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of USER function area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define Uart_START_SEC_CODE
#include "Uart_MemMap.h"
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfunction.
 */
void Uart_InitMemory( void )
{
  uint8 idx = 0;

  /* Initialize Links between data structure which are used for receiving uart data frames */
  for(idx = 0; idx < MAX_AVAILABLE_UART_MODULES; idx++)
  {
    rxFrameData[idx].pData = &frameByteData[idx][0];
    hlpUartFillBufferDefaultValues(rxFrameData[idx].pData, UART_GLOBAL_MAX_RX_ELEMENTS);
    rxFrameDataLengthCounter[idx] = 0;
    rxFrameData[idx].cntOfData = UART_GLOBAL_MAX_RX_ELEMENTS;
  }
}




/* ######################################################################################################################## */
/* ######################################## UART PROVIDED HIGH LEVEL INTERFACES ########################################### */
/* ######################################################################################################################## */
/**
 * @brief           API function to set a transmit data frame
 * @param[in]       UartIf_TxFrameType   Uart layer frame data
 * @retval          Std_ReturnType
 *                  >> E_OK
 *                  >> E_NOT_OK
 *                  >> E_PENDING
 *                  >> E_SKIPPED
 * @details         This function could be used to set a new transmit data frame from the uart module
 */
Std_ReturnType Uart_Transmit(UartIf_TxFrameType frameData)
{
  Std_ReturnType retVal;
  HAL_StatusTypeDef statusType;

  /* Check Input parameters */
  if(UART_CNT_OF_USED_MODULES <= frameData.moduleID)
  {
#if (STD_ON == UART_MODULE_USE_DET)
     Det_ReportError(UART_DET_MODULE_ID, UART_E_INVALID_PARAMETER);
#endif
     return E_NOT_OK;
  }

  if(NULL == frameData.cntOfData)
  {
#if (STD_ON == UART_MODULE_USE_DET)
     Det_ReportError(UART_DET_MODULE_ID, UART_E_INVALID_PARAMETER);
#endif
     return E_NOT_OK;
  }

  if(NULL_PTR == frameData.pData)
  {
#if (STD_ON == UART_MODULE_USE_DET)
     Det_ReportError(UART_DET_MODULE_ID, UART_E_NULL_POINTER);
#endif
     return E_NOT_OK;
  }

  /* Check if interrupt or polling processing shall be used */
  switch(uartHwModuleCfg[frameData.moduleID].uartTransmissionType)
  {
  /* ---------------------------------------------------------------------------------------------- */
  /* -------------------------- Processing interrupt transmission --------------------------------- */
  /* ---------------------------------------------------------------------------------------------- */
  case MCAL_UART_TRANSMISSION_INTERRUPT:
    statusType = HAL_UART_Transmit_IT(&halUartHdlTypes[frameData.moduleID], frameData.pData, frameData.cntOfData);
    if( HAL_OK != statusType)
    {
      #if (STD_ON == UART_MODULE_USE_DET)
      if(HAL_TIMEOUT == statusType)
      { Det_ReportError(UART_DET_MODULE_ID, UART_E_UNEXPECTED_HAL_MODULE_STATE); }
      retVal = E_NOT_OK;
      #endif
      if(HAL_BUSY == statusType)
      {
        retVal = E_SKIPPED;
      }
      else
      {
        /* Unknown error */
        retVal = E_NOT_OK;
      }
      //{ Det_ReportError(UART_DET_MODULE_ID, UART_E_UNEXPECTED_HAL_MODULE_STATE); }
    }
    else
    {
      retVal = E_PENDING;
    }
    
    break;

  /* ---------------------------------------------------------------------------------------------- */
  /* -------------------------- Processing polling transmission ----------------------------------- */
  /* ---------------------------------------------------------------------------------------------- */
  case MCAL_UART_TRANSMISSION_POLLING:
    /* Start Tx transmission */
    statusType = HAL_UART_Transmit(&halUartHdlTypes[frameData.moduleID], frameData.pData, frameData.cntOfData, uartHwModuleCfg[frameData.moduleID].uartTxTimeout);
    if( HAL_OK != statusType)
    {
#if (STD_ON == UART_MODULE_USE_DET)
      if(HAL_TIMEOUT == statusType)
      { Det_ReportError(UART_DET_MODULE_ID, UART_E_TIMEOUT); }
      if(HAL_BUSY == statusType)
      { Det_ReportError(UART_DET_MODULE_ID, UART_E_UNEXPECTED_HAL_MODULE_STATE); }
#endif
      retVal = E_NOT_OK;
    }
    else
    {
      /* Call Tx Notification */
      UartIf_TxNotification(frameData.moduleID);
      retVal = E_OK;
    }
    break;

  /* ---------------------------------------------------------------------------------------------- */
  /* ----------------------------- ERROR >> Invalid configuration --------------------------------- */
  /* ---------------------------------------------------------------------------------------------- */
  default:
#if (STD_ON == UART_MODULE_USE_DET)
     Det_ReportError(UART_DET_MODULE_ID, UART_E_GENERIC_PROGRAMMING_FAILURE);
#endif
     retVal = E_NOT_OK;
    break;
  }

  return retVal;
}

/* ######################################################################################################################## */
/* ###################################### UART PROVIDED SYSTEM LEVEL INTERFACES ########################################### */
/* ######################################################################################################################## */
/**
 * @brief           Interrupt Service Routine for the UART 1
 * @param[in]       None
 * @retval          None
 * @note            Call the HAL interrupt handler
 */
void USART1_IRQHandler(void)
{
  HAL_UART_IRQHandler(&halUartHdlTypes[0]);
}

/**
 * @brief           Interrupt Service Routine for the UART 2
 * @param[in]       None
 * @retval          None
 * @note            Call the HAL interrupt handler
 */
void USART2_IRQHandler(void)
{
  HAL_UART_IRQHandler(&halUartHdlTypes[1]);
}

/**
 * @brief           Interrupt Service Routine for the UART 3
 * @param[in]       None
 * @retval          None
 * @note            Call the HAL interrupt handler
 */
void USART3_IRQHandler(void)
{
  HAL_UART_IRQHandler(&halUartHdlTypes[2]);
}

/**
 * @brief           Interrupt Service Routine for the UART 4
 * @param[in]       None
 * @retval          None
 * @note            Call the HAL interrupt handler
 */
void UART4_IRQHandler(void)
{
  HAL_UART_IRQHandler(&halUartHdlTypes[3]);
}

/**
 * @brief           Interrupt Service Routine for the UART 5
 * @param[in]       None
 * @retval          None
 * @note            Call the HAL interrupt handler
 */
void UART5_IRQHandler(void)
{
  HAL_UART_IRQHandler(&halUartHdlTypes[4]);
}

/**
 * @brief           Interrupt Service Routine for the UART 6
 * @param[in]       None
 * @retval          None
 * @note            Call the HAL interrupt handler
 */
void USART6_IRQHandler(void)
{
  HAL_UART_IRQHandler(&halUartHdlTypes[5]);
}
/* ######################################################################################################################## */
/* ####################################### PRIVATE UART SYSTEM LEVEL INTERFACES ########################################### */
/* ######################################################################################################################## */
/**
 * @brief           Callback function from the HAL Uart Module RX Interrupts
 * @param[in]       Uart Handle Type Def
 * @retval          None
 */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Check Tx Callback and report them to higher layer */
  if(huart->Instance == halUartHdlTypes[0].Instance)
  {
    UartIf_TxIsrNotification(0);
  }
  else if(huart->Instance == halUartHdlTypes[1].Instance)
  {
    UartIf_TxIsrNotification(1);
  }
  else if(huart->Instance == halUartHdlTypes[2].Instance)
  {
    UartIf_TxIsrNotification(2);
  }
  else if(huart->Instance == halUartHdlTypes[3].Instance)
  {
    UartIf_TxIsrNotification(3);
  }
  else if(huart->Instance == halUartHdlTypes[4].Instance)
  {
    UartIf_TxIsrNotification(4);
  }
  else if(huart->Instance == halUartHdlTypes[5].Instance)
  {
    UartIf_TxIsrNotification(5);
  }
  else
  {
    Det_ReportError(UART_DET_MODULE_ID, UART_E_GENERIC_PROGRAMMING_FAILURE);
  }
}

/**
 * @brief           Callback function from the HAL Uart Module for RX Interrupts
 * @param[in]       Uart Handle Type Def
 * @retval          None
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Check Tx Callback and report them to higher layer */
  if(huart->Instance == halUartHdlTypes[0].Instance)
  {
    hlpUartCheckForRxFrameEndPatternInIsr(0);
    HAL_UART_Receive_IT(huart, &rxFrameData[0].pData[rxFrameDataLengthCounter[0]], 1);   //Activate UART receive interrupt every time
  }
  else if(huart->Instance == halUartHdlTypes[1].Instance)
  {
    hlpUartCheckForRxFrameEndPatternInIsr(1);
    HAL_UART_Receive_IT(huart, &rxFrameData[1].pData[rxFrameDataLengthCounter[1]], 1);   //Activate UART receive interrupt every time
  }
  else if(huart->Instance == halUartHdlTypes[2].Instance)
  {
    hlpUartCheckForRxFrameEndPatternInIsr(2);
    HAL_UART_Receive_IT(huart, &rxFrameData[2].pData[rxFrameDataLengthCounter[2]], 1);   //Activate UART receive interrupt every time
  }
  else if(huart->Instance == halUartHdlTypes[3].Instance)
  {
    hlpUartCheckForRxFrameEndPatternInIsr(3);
    HAL_UART_Receive_IT(huart, &rxFrameData[3].pData[rxFrameDataLengthCounter[3]], 1);   //Activate UART receive interrupt every time
  }
  else if(huart->Instance == halUartHdlTypes[4].Instance)
  {
    hlpUartCheckForRxFrameEndPatternInIsr(4);
    HAL_UART_Receive_IT(huart, &rxFrameData[4].pData[rxFrameDataLengthCounter[4]], 1);   //Activate UART receive interrupt every time
  }
  else if(huart->Instance == halUartHdlTypes[5].Instance)
  {
    hlpUartCheckForRxFrameEndPatternInIsr(5);
    HAL_UART_Receive_IT(huart, &rxFrameData[5].pData[rxFrameDataLengthCounter[5]], 1);   //Activate UART receive interrupt every time
  }
  else
  {
    Det_ReportError(UART_DET_MODULE_ID, UART_E_GENERIC_PROGRAMMING_FAILURE);
  }
}

/* ######################################################################################################################## */
/* ################################################ PRIVATE UART FUNCTIONS ################################################ */
/* ######################################################################################################################## */
/**
 * @brief           Helper function to fill a buffer with default values
 * @param[in]       uint8* Pointer to buffer
 * @param[in]       uint16 Buffer Length
 * @retval          none
 */
void hlpUartFillBufferDefaultValues(uint8* pBuffer, uint16 length)
{
  uint16 idx;
  uint8 defaultValues[2] = {0x52,0x53};


  if( 0 == (length % 2) )
  {
    for(idx = 0; idx < length; (idx+=2))
    {
      memcpy(&pBuffer[idx], &defaultValues[0], 2);
      //pBuffer[idx] = (uint16)UART_FRAME_END_PATTERN;
    }

  }
  else
  {
    for(idx = 0; idx < (length-1); (idx+=2))
    {
      memcpy(&pBuffer[idx], &defaultValues[0], 2);
      //pBuffer[idx] = (uint16)UART_FRAME_END_PATTERN;
    }
    pBuffer[length] = 0x00;
  }

}

/**
 * @brief           Helper function to search for an end pattern
 * @param[in]       uint8* Pointer to buffer
 * @param[in]       uint16 Buffer Length
 * @retval          uint16 Length of buffer data with end pattern
 */
uint16 hlpUartFindBufferEndPattern(uint8* pBuffer, uint16 length)
{
  uint16 idx, idx2;
  boolean equal = TRUE;


  if(length >= UART_FRAME_END_PATTERN_LENGTH)
  {
    for(idx = 0; idx < length; idx++)
    {
      equal = TRUE;

      for(idx2 = 0; idx2 < UART_FRAME_END_PATTERN_LENGTH; idx2++)
      {
        /* Check for buffer overflow */
        if((idx + idx2) >=  length)
        {
          equal = FALSE;
          break;
        }

        /* Check end pattern */
        if(pBuffer[idx + idx2] != mcalUartPduEndPattern[idx2])
        {
          equal = FALSE;
          break;
        }
      }

      if(TRUE == equal)
      { return (idx + UART_FRAME_END_PATTERN_LENGTH); }
    }
  }
  /* Return dummy value if no end pattern has been found */
  return 0xFFFF;
}


/**
 * @brief           Helper function check for received data within interrupt processing
 * @param[in]       uint8 module id
 * @retval          None
 */
void hlpUartCheckForRxFrameEndPatternInIsr(uint8 moduleId)
{
  uint16 tempBufferDataLength;
  rxFrameDataLengthCounter[moduleId]++;
  if(rxFrameDataLengthCounter[moduleId] >= uartHwModuleCfg[moduleId].uartMaxRxBytes)
  {
    /* Complete frame received without frame end pattern >> Report frame to interface module*/
    UartIf_RxIsrNotification(rxFrameData[moduleId]);
  }

  /* Max frame length not reached >> Check for frame end pattern */

  tempBufferDataLength = hlpUartFindBufferEndPattern(&rxFrameData[moduleId].pData[0], rxFrameDataLengthCounter[moduleId]);

  /* Check if end pattern has been found */
  if(0xFFFF != tempBufferDataLength)
  {
    /* Buffer end pattern has been found >> Set new length >> Report frame to interface module */
    rxFrameData[moduleId].cntOfData = tempBufferDataLength;
    UartIf_RxIsrNotification(rxFrameData[moduleId]);

    /* Set default values to data */
    hlpUartFillBufferDefaultValues(rxFrameData[0].pData, rxFrameDataLengthCounter[moduleId]);
    rxFrameDataLengthCounter[moduleId] = 0; /* Reset Data counter */
    rxFrameData[moduleId].cntOfData = UART_GLOBAL_MAX_RX_ELEMENTS;
  }
  else
  {
    /* Frame end pattern not found >> Continue with processing */
  }
}

#define Uart_STOP_SEC_CODE
#include "Uart_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of USER function area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

