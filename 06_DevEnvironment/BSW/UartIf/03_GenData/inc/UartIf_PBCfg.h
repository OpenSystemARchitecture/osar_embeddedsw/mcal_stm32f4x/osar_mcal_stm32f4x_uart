/*****************************************************************************************************************************
 * @file        UartIf_PBCfg.h                                                                                               *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        08.12.2020 15:23:00                                                                                          *
 * @brief       Generated header file data of the UartIf module.                                                             *
 * @version     v.1.1.1                                                                                                      *
 * @generator   OSAR UartIf Generator v.1.0.0.1                                                                              *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.7.1                                                                           *
*****************************************************************************************************************************/

#ifndef __UARTIF_PBCFG_H
#define __UARTIF_PBCFG_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup UartIf 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
#include "UartIf_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- UartIf Module Det Information ---------------------------------------------*/
#define UARTIF_DET_MODULE_ID               1
#define UARTIF_MODULE_USE_DET              STD_ON
/*-------------------------------------- General Uart Interface Buffer Configuration --------------------------------------*/
#define UARTIF_CNT_OF_USED_MODULES         1
#define UARTIF_CNT_OF_ALL_TX_FRAMES        11
#define UARTIF_CNT_OF_ALL_RX_FRAMES        10
/*---------------------------------- General Uart Interface Buffer Element Configuration ----------------------------------*/
#define UARTIF_MODULE1_TX_CNT_FRAMES       11
#define UARTIF_MODULE1_TX_FRAMES_DATA_SIZE 20
#define UARTIF_MODULE1_RX_CNT_FRAMES       10
#define UARTIF_MODULE1_RX_FRAMES_DATA_SIZE 15
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*---------------------------------------------- UartIf configuration buffer ----------------------------------------------*/
extern const UartIf_ModuleConfigType uartIfModuleConfiguration[UARTIF_CNT_OF_USED_MODULES];

/*------------------------------------------------- UartIf general buffer -------------------------------------------------*/
extern UartIf_Buffer_Frame uartIfFrameTxBuffer[UARTIF_CNT_OF_ALL_TX_FRAMES];

extern UartIf_Buffer_Frame uartIfFrameRxBuffer[UARTIF_CNT_OF_ALL_RX_FRAMES];

/*--------------------------------------------- UartIf general control buffer ---------------------------------------------*/
extern UartIf_Buffer_Ctrl uartIfTxBufferCtrl[UARTIF_CNT_OF_USED_MODULES];

extern UartIf_Buffer_Ctrl uartIfRxBufferCtrl[UARTIF_CNT_OF_USED_MODULES];

/*---------------------------------------- UartIf module specific frame data buffer ----------------------------------------*/
extern uint8 uartIfFrameDataTxModule1[11][21];

extern uint8 uartIfFrameDataRxModule1[10][16];

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __UARTIF_PBCFG_H*/
