/*****************************************************************************************************************************
 * @file        UartIf_PBCfg.c                                                                                               *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        08.12.2020 15:23:00                                                                                          *
 * @brief       Implementation of UartIf module *_PBCfg.c file                                                               *
 * @version     v.1.1.1                                                                                                      *
 * @generator   OSAR UartIf Generator v.1.0.0.1                                                                              *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.7.1                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup UartIf 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "UartIf_PBCfg.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define UartIf_START_SEC_ZERO_INIT_VAR
#include "UartIf_MemMap.h"
/**
 * @brief           Uart module specific frame buffer data
 */
uint8 uartIfFrameDataTxModule1[11][21];

uint8 uartIfFrameDataRxModule1[10][16];

#define UartIf_STOP_SEC_ZERO_INIT_VAR
#include "UartIf_MemMap.h"
/*------------------------------------------- Map variables into constant memory -------------------------------------------*/
#define UartIf_START_SEC_CONST
#include "UartIf_MemMap.h"
/**
 * @brief           General Uart module configuration data
 */
const UartIf_ModuleConfigType uartIfModuleConfiguration[1] = {
  { NULL_PTR, NULL_PTR, NULL_PTR, NULL_PTR }
};

#define UartIf_STOP_SEC_CONST
#include "UartIf_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define UartIf_START_SEC_INIT_VAR
#include "UartIf_MemMap.h"
/**
 * @brief           General Frame Buffer for Tx Modules
 */
UartIf_Buffer_Frame uartIfFrameTxBuffer[UARTIF_CNT_OF_ALL_TX_FRAMES] = {
  { 0, &uartIfFrameDataTxModule1[0][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[1][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[2][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[3][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[4][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[5][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[6][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[7][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[8][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[9][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataTxModule1[10][0], UART_FRAME_INVALID }
};

/**
 * @brief           General Frame Buffer for Rx Modules
 */
UartIf_Buffer_Frame uartIfFrameRxBuffer[UARTIF_CNT_OF_ALL_RX_FRAMES] = {
  { 0, &uartIfFrameDataRxModule1[0][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[1][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[2][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[3][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[4][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[5][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[6][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[7][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[8][0], UART_FRAME_INVALID },
  { 0, &uartIfFrameDataRxModule1[9][0], UART_FRAME_INVALID }
};

/**
 * @brief           Control data of the different Uart Tx buffers
 */
UartIf_Buffer_Ctrl uartIfTxBufferCtrl[1] = {
  { UARTIF_MODULE1_TX_CNT_FRAMES, 0, 0, UARTIF_MODULE1_TX_FRAMES_DATA_SIZE, 0 }
};

/**
 * @brief           Control data of the different Uart Rx buffers
 */
UartIf_Buffer_Ctrl uartIfRxBufferCtrl[1] = {
  { UARTIF_MODULE1_RX_CNT_FRAMES, 0, 0, UARTIF_MODULE1_RX_FRAMES_DATA_SIZE, 0 }
};

#define UartIf_STOP_SEC_INIT_VAR
#include "UartIf_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

