/*****************************************************************************************************************************
 * @file        UartIf.h                                                                                                     *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.02.2018 07:31:55                                                                                          *
 * @brief       Implementation of definitions / interface function prototypes / datatypes and generic module interface       *
 *              informations of the "UartIf" module.                                                                         *
 *                                                                                                                           *
 * @details     The Uart Interface Module (UartIf) is the central handler module for the Uart. It works with and Frame       *
 *              architecture. For each configured Uart module, the UartIf would create a user adaptable ring buffer. For     *
 *              each module a configurable TX Frame and RX Frame ring buffer would be generated. The user could decide how   *
 *              many frames the UartIf shall buffer and the frames size could also be adapted.                               *
 *                                                                                                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
#ifndef __UARTIF_H
#define __UARTIF_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup UartIf
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "UartIf_Types.h"
#include "UartIf_PBCfg.h"
#include "UartIf_HiIf.h"
#include "UartIf_LoIf.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfunction.
 */
void UartIf_InitMemory( void );

/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called befor using the module and after the memory initialization.
 */
void UartIf_Init( void );

/**
 * @brief           Module global mainfunction.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called form the actual runtime environment within an fixed cycle.
 */
void UartIf_Mainfunction( void );

/* ######################################################################################################################## */
/* ######################################## UART PROVIDED HIGH LEVEL INTERFACES ########################################### */
/* ######################################################################################################################## */

/**
* @brief           API function to read a received data frame
* @param[in/out]   UartIf_RxFrameType   Higher layer frame data
* @retval          UartReturnType
*                  >> UARTIF_E_NULL_POINTER
*                  >> UARTIF_E_BUFFER_EMPTY
*                  >> UARTIF_E_BUFFER_OVERFLOW
*                  >> UARTIF_E_OK
*                  >> UARTIF_E_NOT_OK
* @details         This function could be used to read a receive data frame from an uart module
*/
UartIf_ReturnType UartIf_GetReceivedData( UartIf_RxFrameType *dataFrame );

/**
 * @brief           API function to set a transmit data frame
 * @param[in/out]   UartIf_TxFrameType   Higher layer frame data
 * @retval          UartReturnType
 *                  >> UARTIF_E_NULL_POINTER
 *                  >> UARTIF_E_BUFFER_EMPTY
 *                  >> UARTIF_E_BUFFER_OVERFLOW
 *                  >> UARTIF_E_OK
 *                  >> UARTIF_E_NOT_OK
 * @details         This function could be used to set a new transmit data frame for the uart module
 */
UartIf_ReturnType UartIf_SetTransmitData( UartIf_TxFrameType *dataFrame );
/* ######################################################################################################################## */
/* #################################### MCAL UART MANDATORY LOW LEVEL INTERFACES ########################################## */
/* ######################################################################################################################## */
/**
 * @brief           API function to notify the uart module about an tx interrupt from a specific module
 * @param[in]       uint8 module id
 * @retval          Std_ReturnType
 * @details         This function could be used to notify the uart about an tx interrupt. It would inform the necessary high
 *                  level module about the interrupt if this is configured.
 * @note            This function has to be called in the Interrupt service routine of the corresponding module.
 */
Std_ReturnType UartIf_TxIsrNotification( uint8 moduleId );

/**
 * @brief           API function to notify the uart module about an rx interrupt from a specific module
 * @param[in]       UartIf_RxFrameType received frame data
 * @retval          Std_ReturnType
 * @details         This function could be used to notify the uart about an rx interrupt. It would inform the necessary high
 *                  level module about the interrupt if this is configured.
 * @note            This function has to be called in the Interrupt service routine of the corresponding module.
 */
Std_ReturnType UartIf_RxIsrNotification( UartIf_RxFrameType frameData );

/**
 * @brief           API function to notify the uart module about an tx event from a specific module
 * @param[in]       uint8 module id
 * @retval          Std_ReturnType
 * @details         This function could be used to notify the uart about an tx event. It would inform the necessary high
 *                  level module about the event if this is configured.
 */
Std_ReturnType UartIf_TxNotification( uint8 moduleId );

/**
* @brief           API function to notify the uart module about an rx event from a specific module
* @param[in]       UartIf_RxFrameType received frame data
* @retval          Std_ReturnType
* @details         This function could be used to notify the uart about an rx event. It would inform the necessary high
*                  level module about the event if this is configured.
*/
Std_ReturnType UartIf_RxNotification( UartIf_RxFrameType frameData );
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __UARTIF_H*/
