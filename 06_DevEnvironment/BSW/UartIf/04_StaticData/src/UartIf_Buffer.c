/*****************************************************************************************************************************
 * @file        Uart_Buffer.c                                                                                                *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        18.02.2018 11:52:01                                                                                          *
 * @brief       Implementation of functionalities from the "Uart_Buffer" module.                                             *
 *                                                                                                                           *
 * @details     The uart buffer module implements the TX / RX Ring buffer handling for the higher layer modules also for the *
 *              Mcal_Uart module. The buffer itself would be generated using the UartGenerator. The frame data length also   *
 *              as th count of frames is adaptable.                                                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup UartIf
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "UartIf_Buffer.h"

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define UartIf_START_SEC_CONST
#include "UartIf_MemMap.h"
#define UartIf_STOP_SEC_CONST
#include "UartIf_MemMap.h"
/*----------------------------------------- Map variables into uninitalized memory -----------------------------------------*/
#define UartIf_START_SEC_NOINIT_VAR
#include "UartIf_MemMap.h"
UartIf_ReturnType uartIfBufferStatus;
#define UartIf_STOP_SEC_NOINIT_VAR
#include "UartIf_MemMap.h"
/*------------------------------------------ Map variables into initalized memory ------------------------------------------*/
#define UartIf_START_SEC_INIT_VAR
#include "UartIf_MemMap.h"
#define UartIf_STOP_SEC_INIT_VAR
#include "UartIf_MemMap.h"
/*--------------------------------------- Map variables into zero initalized memory ---------------------------------------*/
#define UartIf_START_SEC_ZERO_INIT_VAR
#include "UartIf_MemMap.h"
#define UartIf_STOP_SEC_ZERO_INIT_VAR
#include "UartIf_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define UartIf_START_SEC_CONST
#include "UartIf_MemMap.h"
#define UartIf_STOP_SEC_CONST
#include "UartIf_MemMap.h"
/*----------------------------------------- Map variables into uninitalized memory -----------------------------------------*/
#define UartIf_START_SEC_NOINIT_VAR
#include "UartIf_MemMap.h"
#define UartIf_STOP_SEC_NOINIT_VAR
#include "UartIf_MemMap.h"
/*------------------------------------------ Map variables into initalized memory ------------------------------------------*/
#define UartIf_START_SEC_INIT_VAR
#include "UartIf_MemMap.h"
#define UartIf_STOP_SEC_INIT_VAR
#include "UartIf_MemMap.h"
/*--------------------------------------- Map variables into zero initalized memory ---------------------------------------*/
#define UartIf_START_SEC_ZERO_INIT_VAR
#include "UartIf_MemMap.h"
#define UartIf_STOP_SEC_ZERO_INIT_VAR
#include "UartIf_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define UartIf_START_SEC_CODE
#include "UartIf_MemMap.h"
#define UartIf_STOP_SEC_CODE
#include "UartIf_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------- Uart buffer functions --------------------------------------------------*/
#define UartIf_START_SEC_CODE
#include "UartIf_MemMap.h"
/**
 * @brief           Init the uart buffer memory with an valid configuration
 * @param[]         None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfunction.
 */
void UartIf_Buffer_InitMemory(void)
{
  /* Set Uart buffer status */
  uartIfBufferStatus = UARTIF_E_OK;
}

/**
 * @brief           Request the actual RX Buffer load from an specific module
 * @param[in]       uint8 module ID
 * @param[out]      uint8* pActualLoad  >> Output variable
 * @retval          UartIf_ReturnType
 */
UartIf_ReturnType UartIf_Buffer_GetActualRxBufferLoad(uint8 moduleId, uint8* pActualLoad)
{
  uint8 tempLoad;

  /* Check input parameter */
  if (moduleId >= UARTIF_CNT_OF_USED_MODULES)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_INDEX_OUT_OF_RANGE);
#endif
    return UARTIF_E_NOT_OK;
  }
  if (NULL_PTR == pActualLoad)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_NULL_POINTER);
#endif
    return UARTIF_E_NOT_OK;
  }

  /* Calculate actual load */
  if(uartIfRxBufferCtrl[moduleId].readElementPos < uartIfRxBufferCtrl[moduleId].writeElementPos)
  {
    tempLoad = (uartIfRxBufferCtrl[moduleId].writeElementPos - uartIfRxBufferCtrl[moduleId].readElementPos);

    /* Check if buffer is full */
    if (tempLoad == (uartIfRxBufferCtrl[moduleId].cntOfBufferFrames -1) )
    {
      *pActualLoad = tempLoad;
      return UARTIF_E_BUFFER_FULL;
    }
    else
    {
      *pActualLoad = tempLoad;
      return UARTIF_E_OK;
    }
  }
  else if (uartIfRxBufferCtrl[moduleId].readElementPos == uartIfRxBufferCtrl[moduleId].writeElementPos)
  {
    *pActualLoad = 0;
    return UARTIF_E_BUFFER_EMPTY;
  }
  else if (uartIfRxBufferCtrl[moduleId].readElementPos > uartIfRxBufferCtrl[moduleId].writeElementPos)
  {
    tempLoad = uartIfRxBufferCtrl[moduleId].cntOfBufferFrames - uartIfRxBufferCtrl[moduleId].readElementPos;
    tempLoad += uartIfRxBufferCtrl[moduleId].writeElementPos;

    /* Check if buffer is full */
    if (tempLoad == (uartIfRxBufferCtrl[moduleId].cntOfBufferFrames - 1))
    {
      *pActualLoad = tempLoad;
      return UARTIF_E_BUFFER_FULL;
    }
    else
    {
      *pActualLoad = tempLoad;
      return UARTIF_E_OK;
    }
  }

  return UARTIF_E_NOT_OK;
}

/**
 * @brief           Request the actual TX Buffer load from an specific module
 * @param[in]       uint8 module ID
 * @param[out]      uint8* pActualLoad  >> Output variable
 * @retval          UartIf_ReturnType
 *                  > UARTIF_E_NOT_OK
 *                  > UARTIF_E_BUFFER_FULL
 *                  > UARTIF_E_BUFFER_EMPTY
 *                  > UARTIF_E_OK
 */
UartIf_ReturnType UartIf_Buffer_GetActualTxBufferLoad(uint8 moduleId, uint8* pActualLoad)
{
  uint8 tempLoad;

  /* Check input parameter */
  if (moduleId >= UARTIF_CNT_OF_USED_MODULES)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_INDEX_OUT_OF_RANGE);
#endif
    return UARTIF_E_NOT_OK;
  }
  if (NULL_PTR == pActualLoad)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_NULL_POINTER);
#endif
    return UARTIF_E_NOT_OK;
  }

  /* Calculate actual load */
  if (uartIfTxBufferCtrl[moduleId].readElementPos < uartIfTxBufferCtrl[moduleId].writeElementPos)
  {
    tempLoad = (uartIfTxBufferCtrl[moduleId].writeElementPos - uartIfTxBufferCtrl[moduleId].readElementPos);

    /* Check if buffer is full */
    if (tempLoad == (uartIfTxBufferCtrl[moduleId].cntOfBufferFrames - 1))
    {
      *pActualLoad = tempLoad;
      return UARTIF_E_BUFFER_FULL;
    }
    else
    {
      *pActualLoad = tempLoad;
      return UARTIF_E_OK;
    }
  }
  else if (uartIfTxBufferCtrl[moduleId].readElementPos == uartIfTxBufferCtrl[moduleId].writeElementPos)
  {
    *pActualLoad = 0;
    return UARTIF_E_BUFFER_EMPTY;
  }
  else if (uartIfTxBufferCtrl[moduleId].readElementPos > uartIfTxBufferCtrl[moduleId].writeElementPos)
  {
    tempLoad = uartIfTxBufferCtrl[moduleId].cntOfBufferFrames - uartIfTxBufferCtrl[moduleId].readElementPos;
    tempLoad += uartIfTxBufferCtrl[moduleId].writeElementPos;

    /* Check if buffer is full */
    if (tempLoad == (uartIfTxBufferCtrl[moduleId].cntOfBufferFrames - 1))
    {
      *pActualLoad = tempLoad;
      return UARTIF_E_BUFFER_FULL;
    }
    else
    {
      *pActualLoad = tempLoad;
      return UARTIF_E_OK;
    }
  }

  return UARTIF_E_NOT_OK;
}

/**
* @brief           Request the actual RX Buffer Frame Read Element
* @param[in]       uint8 module ID
* @param[out]      UartIf_Buffer_Frame *pBufferFrame >> Output variable
* @retval          UartIf_ReturnType
*                  > UARTIF_E_NOT_OK
*                  > UARTIF_E_BUFFER_EMPTY
*                  > UARTIF_E_BUFFER_OVERFLOW
*                  > UARTIF_E_PENDING
*                  > UARTIF_E_OK
*/
UartIf_ReturnType UartIf_Buffer_GetRxReadFramePointer(uint8 moduleId, UartIf_Buffer_Frame **pBufferFrame)
{
  uint8 tempLoad;
  /* Check input parameter */
  if (moduleId >= UARTIF_CNT_OF_USED_MODULES)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_INDEX_OUT_OF_RANGE);
#endif

    return UARTIF_E_NOT_OK;
  }
  if (NULL_PTR == pBufferFrame)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_NULL_POINTER);
#endif
    return UARTIF_E_NOT_OK;
  }

  /* Check if buffer is empty */
  if(UARTIF_E_BUFFER_EMPTY == UartIf_Buffer_GetActualRxBufferLoad(moduleId, &tempLoad))
  {
    *pBufferFrame = NULL_PTR;
    return UARTIF_E_BUFFER_EMPTY;
  }
  else
  {
    /* Check buffer end pattern */
    if (0x00 != uartIfFrameRxBuffer[uartIfRxBufferCtrl[moduleId].frameBufferStartIdx + uartIfRxBufferCtrl[moduleId].readElementPos].pToActualFrameData[uartIfRxBufferCtrl[moduleId].sizeOfBufferFrameData])
    {
      /* Buffer overflow detected >> Report error */
#if(STD_ON == UARTIF_MODULE_USE_DET)
      Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_BUFFER_OVERFLOW);
#endif
      uartIfBufferStatus = UARTIF_E_BUFFER_OVERFLOW;
      *pBufferFrame = NULL_PTR;
      return UARTIF_E_BUFFER_OVERFLOW;
    }
    else if ((UART_FRAME_PENDING == uartIfFrameRxBuffer[uartIfRxBufferCtrl[moduleId].frameBufferStartIdx + uartIfRxBufferCtrl[moduleId].readElementPos].frameStatus))
    {
      /* Element is current pending so set pending */
      *pBufferFrame = NULL_PTR;
      return UARTIF_E_PENDING;
    }
    else
    {
      /* Buffer element OK >> contine with processing */
      /* Set Read Pointer */
      *pBufferFrame = &uartIfFrameRxBuffer[uartIfRxBufferCtrl[moduleId].frameBufferStartIdx + uartIfRxBufferCtrl[moduleId].readElementPos];
      /* Update Read Pointer */
      uartIfRxBufferCtrl[moduleId].readElementPos++;
      if (uartIfRxBufferCtrl[moduleId].readElementPos == uartIfRxBufferCtrl[moduleId].cntOfBufferFrames)
      {
        uartIfRxBufferCtrl[moduleId].readElementPos = 0;
      }
      return UARTIF_E_OK;
    }
  }
  return UARTIF_E_NOT_OK;
}

/**
* @brief           Request the actual TX Buffer Frame Read Element
* @param[in]       uint8 module ID
* @param[out]      UartIf_Buffer_Frame *pBufferFrame >> Output variable
* @retval          UartIf_ReturnType
*/
UartIf_ReturnType UartIf_Buffer_GetTxReadFramePointer(uint8 moduleId, UartIf_Buffer_Frame **pBufferFrame)
{
  uint8 tempLoad;
  /* Check input parameter */
  if (moduleId >= UARTIF_CNT_OF_USED_MODULES)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_INDEX_OUT_OF_RANGE);
#endif
    return UARTIF_E_NOT_OK;
  }
  if (NULL_PTR == pBufferFrame)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_NULL_POINTER);
#endif
    return UARTIF_E_NOT_OK;
  }

  /* Check if buffer is empty */
  if(UARTIF_E_BUFFER_EMPTY == UartIf_Buffer_GetActualTxBufferLoad(moduleId, &tempLoad))
  {
    *pBufferFrame = NULL_PTR;
    return UARTIF_E_BUFFER_EMPTY;
  }
  else if ((UART_FRAME_PENDING == uartIfFrameTxBuffer[uartIfTxBufferCtrl[moduleId].frameBufferStartIdx + uartIfTxBufferCtrl[moduleId].readElementPos].frameStatus))
  {
    /* Element is current pending so set pending */
    *pBufferFrame = NULL_PTR;
    return UARTIF_E_PENDING;
  }
  else
  {
    /* Check buffer end pattern */
    if (0x00 != uartIfFrameTxBuffer[uartIfTxBufferCtrl[moduleId].frameBufferStartIdx + uartIfTxBufferCtrl[moduleId].readElementPos].pToActualFrameData[uartIfTxBufferCtrl[moduleId].sizeOfBufferFrameData])
    {
      /* Buffer overflow detected >> Report error */
#if(STD_ON == UARTIF_MODULE_USE_DET)
      Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_BUFFER_OVERFLOW);
#endif
      uartIfBufferStatus = UARTIF_E_BUFFER_OVERFLOW;
      *pBufferFrame = NULL_PTR;
      return UARTIF_E_BUFFER_OVERFLOW;
    }
    else
    {
      /* Buffer element OK >> contine with processing */
      /* Set Read Pointer */
      *pBufferFrame = &uartIfFrameTxBuffer[uartIfTxBufferCtrl[moduleId].frameBufferStartIdx + uartIfTxBufferCtrl[moduleId].readElementPos];
      return UARTIF_E_OK;
    }
  }
  return UARTIF_E_NOT_OK;
}

/**
 * @brief           Increment the actual TX Buffer Frame Read Element Index
 * @param[in]       uint8 module ID
 * @retval          UartIf_ReturnType
 *                  > UARTIF_E_NOT_OK
 *                  > UARTIF_E_OK
 */
UartIf_ReturnType UartIf_Buffer_IncrementTxReadFramePointer(uint8 moduleId)
{
  UartIf_ReturnType retVal = UARTIF_E_NOT_OK;
  /* Check input parameter */
  if (moduleId >= UARTIF_CNT_OF_USED_MODULES)
  {
    #if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_INDEX_OUT_OF_RANGE);
    #endif
    retVal = UARTIF_E_NOT_OK;
  }
  else
  {
    /* Update Read Pointer */
    uartIfTxBufferCtrl[moduleId].readElementPos++;
    if (uartIfTxBufferCtrl[moduleId].readElementPos == uartIfTxBufferCtrl[moduleId].cntOfBufferFrames)
    {
      uartIfTxBufferCtrl[moduleId].readElementPos = 0;
    }
    retVal =  UARTIF_E_OK;
  }

  return retVal;
}


/**
* @brief           Request the actual RX Buffer Frame Write Element
* @param[in]       uint8 module ID
* @param[out]      UartIf_Buffer_Frame **pBufferFrame >> Output variable
* @retval          UartIf_ReturnType
*                  > UARTIF_E_NOT_OK
*                  > UARTIF_E_BUFFER_FULL
*                  > UARTIF_E_PENDING
*                  > UARTIF_E_OK
*/
UartIf_ReturnType UartIf_Buffer_GetRxWriteFramePointer(uint8 moduleId, UartIf_Buffer_Frame **pBufferFrame)
{
  uint8 tempLoad;
  /* Check input parameter */
  if (moduleId >= UARTIF_CNT_OF_USED_MODULES)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_INDEX_OUT_OF_RANGE);
#endif
    return UARTIF_E_NOT_OK;
  }
  if (NULL_PTR == pBufferFrame)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_NULL_POINTER);
#endif
    return UARTIF_E_NOT_OK;
  }

  /* Check if buffer is empty */
  if (UARTIF_E_BUFFER_FULL == UartIf_Buffer_GetActualRxBufferLoad(moduleId, &tempLoad))
  {
    *pBufferFrame = NULL_PTR;
    return UARTIF_E_BUFFER_FULL;
  }
  else if ((UART_FRAME_PENDING == uartIfFrameRxBuffer[uartIfRxBufferCtrl[moduleId].frameBufferStartIdx + uartIfRxBufferCtrl[moduleId].writeElementPos].frameStatus))
  {
    /* Element is current pending so set pending */
    *pBufferFrame = NULL_PTR;
    return UARTIF_E_PENDING;    //TODO > Could this state bee set?
  }
  else
  {
    /* Set Write Pointer */
    *pBufferFrame = &uartIfFrameRxBuffer[uartIfRxBufferCtrl[moduleId].frameBufferStartIdx + uartIfRxBufferCtrl[moduleId].writeElementPos];
    /* Update Write Pointer */
    uartIfRxBufferCtrl[moduleId].writeElementPos++;
    if (uartIfRxBufferCtrl[moduleId].writeElementPos == uartIfRxBufferCtrl[moduleId].cntOfBufferFrames)
    {
      uartIfRxBufferCtrl[moduleId].writeElementPos = 0;
    }
    return UARTIF_E_OK;
  }
  return UARTIF_E_NOT_OK;
}

/**
* @brief           Request the actual TX Buffer Frame Read Element
* @param[in]       uint8 module ID
* @param[out]      UartIf_Buffer_Frame **pBufferFrame >> Output variable
* @retval          UartIf_ReturnType
*                  > UARTIF_E_NOT_OK
*                  > UARTIF_E_BUFFER_FULL
*                  > UARTIF_E_PENDING
*                  > UARTIF_E_OK
*/
UartIf_ReturnType UartIf_Buffer_GetTxWriteFramePointer(uint8 moduleId, UartIf_Buffer_Frame **pBufferFrame)
{
  uint8 tempLoad;
  /* Check input parameter */
  if (moduleId >= UARTIF_CNT_OF_USED_MODULES)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_INDEX_OUT_OF_RANGE);
#endif
    return UARTIF_E_NOT_OK;
  }
  if (NULL_PTR == pBufferFrame)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_NULL_POINTER);
#endif
    return UARTIF_E_NOT_OK;
  }

  /* Check if buffer is empty */
  if  ( (UARTIF_E_BUFFER_FULL == UartIf_Buffer_GetActualTxBufferLoad(moduleId, &tempLoad)) ||
        (UART_FRAME_READY == uartIfFrameTxBuffer[uartIfTxBufferCtrl[moduleId].frameBufferStartIdx + uartIfTxBufferCtrl[moduleId].writeElementPos].frameStatus))
  {
    *pBufferFrame = NULL_PTR;
    return UARTIF_E_BUFFER_FULL;
  }
  else if ((UART_FRAME_PENDING == uartIfFrameTxBuffer[uartIfTxBufferCtrl[moduleId].frameBufferStartIdx + uartIfTxBufferCtrl[moduleId].writeElementPos].frameStatus))
  {
    /* Element is current pending so set pending */
    *pBufferFrame = NULL_PTR;
    return UARTIF_E_PENDING;
  }
  else
  {
    /* Set Write Pointer */
    *pBufferFrame = &uartIfFrameTxBuffer[uartIfTxBufferCtrl[moduleId].frameBufferStartIdx + uartIfTxBufferCtrl[moduleId].writeElementPos];
    /* Update Write Pointer */
    uartIfTxBufferCtrl[moduleId].writeElementPos++;
    if (uartIfTxBufferCtrl[moduleId].writeElementPos == uartIfTxBufferCtrl[moduleId].cntOfBufferFrames)
    {
      uartIfTxBufferCtrl[moduleId].writeElementPos = 0;
    }
    return UARTIF_E_OK;
  }
  return UARTIF_E_NOT_OK;
}

/**
* @brief           Request the data frame size of the tx buffer from an specific module
* @param[in]       uint8 module ID
* @param[out]      uint8 *pdataSize
* @retval          UartIf_ReturnType
*/
UartIf_ReturnType UartIf_Buffer_GetRxFrameDataSize(uint8 moduleId, uint8 *pDataSize)
{
  /* Check input parameter */
  if (moduleId >= UARTIF_CNT_OF_USED_MODULES)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_INDEX_OUT_OF_RANGE);
#endif
    return UARTIF_E_NOT_OK;
  }
  if (NULL_PTR == pDataSize)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_NULL_POINTER);
#endif
    return UARTIF_E_NOT_OK;
  }

  /* Set data size */
  *pDataSize = uartIfRxBufferCtrl[moduleId].sizeOfBufferFrameData;
  return UARTIF_E_OK;
}

/**
* @brief           Request the data frame size of the tx buffer from an specific module
* @param[in]       uint8 module ID
* @param[out]      uint8 *pdataSize
* @retval          UartIf_ReturnType
*/
UartIf_ReturnType UartIf_Buffer_GetTxFrameDataSize(uint8 moduleId, uint8 *pDataSize)
{
  /* Check input parameter */
  if (moduleId >= UARTIF_CNT_OF_USED_MODULES)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_INDEX_OUT_OF_RANGE);
#endif
    return UARTIF_E_NOT_OK;
  }
  if (NULL_PTR == pDataSize)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_NULL_POINTER);
#endif
    return UARTIF_E_NOT_OK;
  }

  /* Set data size */
  *pDataSize = uartIfTxBufferCtrl[moduleId].sizeOfBufferFrameData;
  return UARTIF_E_OK;
}


#define UartIf_STOP_SEC_CODE
#include "UartIf_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */
