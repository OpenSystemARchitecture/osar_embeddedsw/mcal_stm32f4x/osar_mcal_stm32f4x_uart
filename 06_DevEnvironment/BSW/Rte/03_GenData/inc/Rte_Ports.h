/*****************************************************************************************************************************
 * @file        Rte_Ports.h                                                                                                  *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        20.03.2019 09:52:10                                                                                          *
 * @brief       Generated Rte Module Interface Header File: Rte_Ports.h                                                      *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.10                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

#ifndef __RTE_PORTS_H
#define __RTE_PORTS_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Rte 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_Interfaces.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of available Server Port Error Information ++++++++++++++++++++++++++++++ */
#define Rte_ErrorType_PpPortsDio_Dio_SetPinState_NotOk    100
#define Rte_ErrorType_PpPortsDio_Dio_GetPinState_NotOk    100
#define Rte_ErrorType_PpPortsDio_Dio_TogglePinState_NotOk 100
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of available Server Port Error Information ++++++++++++++++++++++++++++++ */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++++++++++ Generation of Init Runnable Prototypes ++++++++++++++++++++++++++++++++++++++++ */
VAR(void) Ports_Init( VAR(void) );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++++++++++++++ Generation of Cyclic Runnable Prototypes +++++++++++++++++++++++++++++++++++++++ */
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++++++++++++ Generation of Server Port Runnable Prototypes ++++++++++++++++++++++++++++++++++++ */
VAR(Rte_ErrorType) Rte_Server_PpPortsDio_Dio_SetPinState( VAR(uint16) channelId, VAR(Dio_PinState) pinState );

VAR(Rte_ErrorType) Rte_Server_PpPortsDio_Dio_GetPinState( VAR(uint16) channelId, P2VAR(Dio_PinState) pinState );

VAR(Rte_ErrorType) Rte_Server_PpPortsDio_Dio_TogglePinState( VAR(uint16) channelId );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of Client Port Runnable Prototype Information ++++++++++++++++++++++++++++++ */
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++ Generation of Sender Receiver Port Runnable Prototype Information ++++++++++++++++++++++++++ */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __RTE_PORTS_H*/
