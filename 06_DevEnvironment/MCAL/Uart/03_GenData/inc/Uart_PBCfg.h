/*****************************************************************************************************************************
 * @file        Uart_PBCfg.h                                                                                                 *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        09.12.2020 15:24:33                                                                                          *
 * @brief       Generated header file data of the Uart module.                                                               *
 * @version     v.1.1.1                                                                                                      *
 * @generator   OSAR Uart Generator v.1.0.0.1                                                                                *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.8.1                                                                           *
*****************************************************************************************************************************/

#ifndef __UART_PBCFG_H
#define __UART_PBCFG_H

/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Uart 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Uart_Types.h"
#include "stm32f4xx_hal.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*---------------------------------------------- Uart Module Det Information ----------------------------------------------*/
#define UART_DET_MODULE_ID            2
#define UART_MODULE_USE_DET           STD_ON
/*---------------------------------------- General module configuration parameters ----------------------------------------*/
#define UART_CNT_OF_USED_MODULES      1
#define UART_FRAME_END_PATTERN_LENGTH 2
#define UART_GLOBAL_MAX_RX_ELEMENTS   100
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*----------------------------------------------- Uart configuration buffer -----------------------------------------------*/
extern UART_HandleTypeDef halUartHdlTypes[];

extern Uart_HwConfigType uartHwModuleCfg[];

extern const uint8 mcalUartPduEndPattern[];

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __UART_PBCFG_H*/
